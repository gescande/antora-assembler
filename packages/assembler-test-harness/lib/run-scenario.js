'use strict'

const produceAggregateDocuments = require('@antora/assembler/produce-aggregate-documents')
const { expect } = require('chai')
const fsp = require('node:fs/promises')
const loadScenario = require('./load-scenario')
const ospath = require('node:path')

// Q: does this belong in @antora/assembler/test/test-helper.js?
async function runScenario (name, dirname) {
  const { loadAsciiDoc, dir, contentCatalog, assemblerConfig } = await loadScenario(name, dirname)
  const results = produceAggregateDocuments(loadAsciiDoc, contentCatalog, assemblerConfig)
  expect(results).to.exist()
  expect(results).to.not.be.empty()
  for (const result of results) {
    expect(result).to.have.property('src')
    const expected = await fsp.readFile(ospath.join(dir, 'expects', result.src.basename), 'utf8').catch(() => '')
    expect(result.contents.toString()).to.eql(expected)
  }
  return results
}

module.exports = runScenario
