'use strict'

const File = require('vinyl')

// FIXME this needs to be much more comprehensive
function createFile (src, rel) {
  const familySegment = src.family + 's'
  const path = `modules/${src.module}/${familySegment}/${src.relative}`
  if (~src.relative.split('/').findIndex((it) => it.startsWith('_'))) {
    return new File({ path, contents: src.contents, src })
  }
  const moduleRootPath = Array(src.relative.split('/').length - 1)
    .fill('..')
    .join('/')
  const outPath = [
    src.component === 'ROOT' ? '' : src.component,
    src.version,
    src.module === 'ROOT' ? '' : src.module,
    src.family === 'page' ? '' : '_' + familySegment,
    src.family === 'page' ? src.relative.replace(/\.adoc$/, '.html') : src.relative,
  ]
    .filter((it) => it)
    .join('/')
  return new File({
    path,
    contents: src.contents,
    src,
    out: { path: outPath },
    pub: { url: '/' + outPath, moduleRootPath },
    ...(rel && { rel }),
  })
}

module.exports = createFile
