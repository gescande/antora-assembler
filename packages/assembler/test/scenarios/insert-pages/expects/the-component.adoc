= The Component
:revnumber: 1.0
:doctype: book
:underscore: _
:page-component-name: the-component
:page-component-version: 1.0
:page-version: {page-component-version}
:page-component-display-version: 1.0
:page-component-title: The Component

:docname: first-page
:page-module: ROOT
:page-relative-src-path: first-page.adoc
:page-origin-url: https://github.com/acme/the-component
:page-origin-start-path:
:page-origin-refname: v1.0
:page-origin-reftype: branch
:page-origin-refhash: a00000000000000000000000000000000000000z
[#first-page:::]
== First Page Title

contents

:docname: second-page
:page-module: ROOT
:page-relative-src-path: second-page.adoc
:page-origin-url: https://github.com/acme/the-component
:page-origin-start-path:
:page-origin-refname: v1.0
:page-origin-reftype: branch
:page-origin-refhash: a00000000000000000000000000000000000000z
[#second-page:::]
== Second Page Title

contents
