/* eslint-env mocha */
'use strict'

const { expect, heredoc } = require('@antora/assembler-test-harness')
const filterComponentVersions = require('@antora/assembler/filter-component-versions')

describe('filterComponentVersions()', () => {
  let components

  const buildComponents = (spec) =>
    spec.split('\n').map((line) => {
      const [name, versions] = line.split('@')
      const component = { name, versions: versions.split(',').map((version) => ({ name, version })) }
      component.latest = component.versions[0]
      return component
    })

  const buildComponentVersions = (spec) =>
    spec.split('\n').reduce((accum, line) => {
      const [name, versions] = line.split('@')
      return accum.concat(versions.split(',').map((version) => ({ name, version })))
    }, [])

  beforeEach(() => {
    components = buildComponents(heredoc`
      component-a@3.0,2.2,2.1,2.0,1.0
      component-b@1.5
      component-c@2.0,1.0
    `)
  })

  describe('single pattern', () => {
    it('should select all component versions when pattern is *@*', () => {
      const patterns = ['*@*']
      const expected = buildComponentVersions(heredoc`
        component-a@3.0,2.2,2.1,2.0,1.0
        component-b@1.5
        component-c@2.0,1.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select all component versions when pattern is **', () => {
      const patterns = ['**']
      const expected = buildComponentVersions(heredoc`
        component-a@3.0,2.2,2.1,2.0,1.0
        component-b@1.5
        component-c@2.0,1.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select latest component versions when pattern is *', () => {
      const patterns = ['*']
      const expected = buildComponentVersions(heredoc`
        component-a@3.0
        component-b@1.5
        component-c@2.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select exact component version', () => {
      const patterns = ['2.2@component-a']
      const expected = buildComponentVersions('component-a@2.2')
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select latest version of component specified by name', () => {
      const patterns = ['component-a']
      const expected = buildComponentVersions('component-a@3.0')
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select all versions of specified component when version is *', () => {
      const patterns = ['*@component-a']
      const expected = buildComponentVersions('component-a@3.0,2.2,2.1,2.0,1.0')
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select specific version from all component when component is *', () => {
      const patterns = ['2.0@*']
      const expected = buildComponentVersions(heredoc`
        component-a@2.0
        component-c@2.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select specific version from all component when component is not specified', () => {
      const patterns = ['2.0@']
      const expected = buildComponentVersions(heredoc`
        component-a@2.0
        component-c@2.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select latest component versions of components that match component pattern', () => {
      const patterns = ['component-*']
      const expected = buildComponentVersions(heredoc`
        component-a@3.0
        component-b@1.5
        component-c@2.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should match a versionless version when only the component name is specified', () => {
      const patterns = ['component-d']
      components = buildComponents(heredoc`
        component-a@3.0,2.2,2.1,2.0,1.0
        component-d@
      `)
      const expected = buildComponentVersions('component-d@')
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select component versions that match version pattern', () => {
      const patterns = ['2.*@*']
      const expected = buildComponentVersions(heredoc`
        component-a@2.2,2.1,2.0
        component-c@2.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should not match a versionless version when version pattern is specified', () => {
      const patterns = ['*.0@component-d']
      components = buildComponents(heredoc`
        component-a@3.0,2.2,2.1,2.0,1.0
        component-d@,2.0,1.0
      `)
      const expected = buildComponentVersions('component-d@2.0,1.0')
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select component versions that match extglob alternates', () => {
      const patterns = ['1.*@component-@(a|b)']
      const expected = buildComponentVersions(heredoc`
        component-a@1.0
        component-b@1.5
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select component versions that match regexp alternates', () => {
      const patterns = ['1.*@component-(a|b)']
      const expected = buildComponentVersions(heredoc`
        component-a@1.0
        component-b@1.5
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select component versions that match braces alternates', () => {
      const patterns = ['1.*@component-{a,b}']
      const expected = buildComponentVersions(heredoc`
        component-a@1.0
        component-b@1.5
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select component versions with versions that match braces range', () => {
      // FIXME the (1) or (.) is required to work around a bug in picomatch
      const patterns = ['(1).{0..5}@component-*']
      const expected = buildComponentVersions(heredoc`
        component-a@1.0
        component-b@1.5
        component-c@1.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select component names and versions that match braces range', () => {
      // FIXME the (1) or (.) is required to work around a bug in picomatch
      const patterns = ['(1).{0..5}@component-{a..b}']
      const expected = buildComponentVersions(heredoc`
        component-a@1.0
        component-b@1.5
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should support range with multi-digit numbers', () => {
      const patterns = ['{10..30}.0@component-a']
      components = buildComponents('component-a@30.0,20.0,10.0,1.0')
      const expected = buildComponentVersions('component-a@30.0,20.0,10.0')
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should support range with step', () => {
      const patterns = ['*.{0..8..2}@*']
      const expected = buildComponentVersions(heredoc`
        component-a@3.0,2.2,2.0,1.0
        component-c@2.0,1.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })
  })

  describe('multiple patterns', () => {
    it('should select exact component versions', () => {
      const patterns = ['2.2@component-a', '1.0@component-c']
      const expected = buildComponentVersions(heredoc`
        component-a@2.2
        component-c@1.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select latest version of components specified by name', () => {
      const patterns = ['component-a', 'component-c']
      const expected = buildComponentVersions(heredoc`
        component-a@3.0
        component-c@2.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should select more component versions of same component if specified', () => {
      const patterns = ['component-a', '2.*@component-a']
      const expected = buildComponentVersions('component-a@3.0,2.2,2.1,2.0')
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })
  })

  describe('exclusion patterns', () => {
    it('should select latest version of specified components except excluded component', () => {
      const patterns = ['*', '!component-a']
      const expected = buildComponentVersions(heredoc`
        component-b@1.5
        component-c@2.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should exclude matched component versions if pattern begins with !', () => {
      const patterns = ['**', '!*@component-a']
      const expected = buildComponentVersions(heredoc`
        component-b@1.5
        component-c@2.0,1.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should exclude specific component version if pattern begins with !', () => {
      const patterns = ['**', '!1.0@component-a']
      const expected = buildComponentVersions(heredoc`
        component-a@3.0,2.2,2.1,2.0
        component-b@1.5
        component-c@2.0,1.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })

    it('should allow component versions to be excluded, then selected again', () => {
      const patterns = ['**', '!*@component-a', 'component-a']
      const expected = buildComponentVersions(heredoc`
        component-a@3.0
        component-b@1.5
        component-c@2.0,1.0
      `)
      const actual = filterComponentVersions(components, patterns)
      expect(actual).to.eql(expected)
    })
  })
})
