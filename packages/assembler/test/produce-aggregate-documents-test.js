/* eslint-env mocha */
'use strict'

const { expect, loadScenario, runScenario } = require('@antora/assembler-test-harness')
const produceAggregateDocuments = require('@antora/assembler/produce-aggregate-documents')

describe('produceAggregateDocuments()', () => {
  it('should insert page as section of main document', async () => {
    await runScenario('insert-page', __dirname)
  })

  it('should insert pages as sections of main document', async () => {
    await runScenario('insert-pages', __dirname)
  })

  it('should handle navigation entry with external link', async () => {
    await runScenario('external-link', __dirname)
  })

  it('should honor revdate set in component descriptor', async () => {
    const aggregateDocuments = await runScenario('explicit-revdate', __dirname)
    expect(aggregateDocuments).to.have.lengthOf(1)
    expect(aggregateDocuments[0].asciidoc.attributes.revdate).to.equal('2023-06-01')
  })

  it('should create page for each item in single untitled nav menu when root_level is 1', async () => {
    await runScenario('root-level-1-with-bare-nav-items', __dirname)
  })

  it('should handle formatted page title', async () => {
    await runScenario('formatted-page-title', __dirname)
  })

  it('should restore mutable attribute to initial value at end of each page', async () => {
    await runScenario('restore-attribute', __dirname)
  })

  it('should reset mutable attributes at end of each page when document is reduced', async () => {
    await runScenario('reset-attributes-when-reduced', __dirname)
  })

  it('should resolve image targets', async () => {
    await runScenario('resolve-image-targets', __dirname)
  })

  it('should drop output folder for ROOT component', async () => {
    const aggregateDocuments = await runScenario('root-component', __dirname)
    expect(aggregateDocuments).to.have.lengthOf(1)
    expect(aggregateDocuments[0].path).to.equal('home.adoc')
  })

  it('should replace / with :: and . with - in generated IDs', async () => {
    await runScenario('scrub-ids', __dirname)
  })

  it('should rewrite inline anchors', async () => {
    await runScenario('rewrite-inline-anchor', __dirname)
  })

  it('should ignore escaped xrefs', async () => {
    await runScenario('ignore-escaped-xrefs', __dirname)
  })

  it('should rewrite shorthand xref targets', async () => {
    await runScenario('rewrite-shorthand-xref-targets', __dirname)
  })

  it('should rewrite internal xref macro targets', async () => {
    await runScenario('rewrite-internal-xref-macro-targets', __dirname)
  })

  it('should rewrite natural xrefs', async () => {
    await runScenario('rewrite-natural-xrefs', __dirname)
  })

  it('should rewrite page links', async () => {
    await runScenario('rewrite-page-links', __dirname)
  })

  it('should skip commented lines', async () => {
    await runScenario('skip-commented-lines', __dirname)
  })

  it('should change section in page to discrete heading', async () => {
    await runScenario('change-section-to-discrete-heading', __dirname)
  })

  it('should shift levels of multipart page', async () => {
    await runScenario('multipart-page', __dirname)
    const { loadAsciiDoc, contentCatalog, assemblerConfig } = await loadScenario('multipart-page', __dirname)
    const aggregateDocuments = await produceAggregateDocuments(loadAsciiDoc, contentCatalog, assemblerConfig)
    expect(aggregateDocuments).to.have.lengthOf(1)
    expect(aggregateDocuments[0].asciidoc.attributes).to.have.property('doctype', 'article')
  })

  it('should honor leveloffset', async () => {
    await runScenario('honor-leveloffset', __dirname)
  })

  it('should expand relative include', async () => {
    await runScenario('expand-relative-include', __dirname)
  })

  it('should expand nested include', async () => {
    await runScenario('expand-nested-include', __dirname)
  })

  it('should expand nested include with leveloffset', async () => {
    await runScenario('expand-nested-include-leveloffset', __dirname)
  })

  it('should evaluate preprocessor conditional', async () => {
    await runScenario('evaluate-preprocessor-conditional', __dirname)
  })

  it('should ignore lines in a verbatim block', async () => {
    await runScenario('ignore-lines-in-verbatim-block', __dirname)
  })

  it('should process content in AsciiDoc table cell', async () => {
    await runScenario('process-asciidoc-table-cell', __dirname)
  })

  it('should remove source-highlighter attribute on aggregate document if not specified', async () => {
    const { loadAsciiDoc, contentCatalog, assemblerConfig } = await loadScenario('insert-page', __dirname)
    const aggregateDocuments = await produceAggregateDocuments(loadAsciiDoc, contentCatalog, assemblerConfig)
    expect(aggregateDocuments).to.have.lengthOf(1)
    expect(aggregateDocuments[0].asciidoc.attributes).to.not.have.property('source-highlighter')
  })

  it('should only set source-highlighter attribute on aggregate document', async () => {
    await runScenario('source-highlighter', __dirname)
    const { loadAsciiDoc, contentCatalog, assemblerConfig } = await loadScenario('source-highlighter', __dirname)
    const aggregateDocuments = await produceAggregateDocuments(loadAsciiDoc, contentCatalog, assemblerConfig)
    expect(aggregateDocuments).to.have.lengthOf(1)
    expect(aggregateDocuments[0].asciidoc.attributes).to.have.property('source-highlighter', 'rouge')
    expect(aggregateDocuments[0].contents.toString()).to.include('source-highlighter=highlight.js at load time')
  })
})
