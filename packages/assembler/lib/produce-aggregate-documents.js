'use strict'

const File = require('vinyl')
const filterComponentVersions = require('./filter-component-versions')
const produceAggregateDocument = require('./produce-aggregate-document')
const selectMutableAttributes = require('./select-mutable-attributes')

function produceAggregateDocuments (loadAsciiDoc, contentCatalog, assemblerConfig) {
  const { insertStartPage, rootLevel, sectionMergeStrategy, asciidoc: assemblerAsciiDocConfig } = assemblerConfig
  const assemblerAsciiDocAttributes = Object.assign({}, assemblerAsciiDocConfig.attributes)
  const { doctype, revdate, 'source-highlighter': sourceHighlighter } = assemblerAsciiDocAttributes
  delete assemblerAsciiDocAttributes.doctype
  delete assemblerAsciiDocAttributes.revdate
  delete assemblerAsciiDocAttributes['source-highlighter']
  return filterComponentVersions(contentCatalog.getComponents(), assemblerConfig.componentVersions).reduce(
    (accum, componentVersion) => {
      const { name: componentName, version, title, navigation } = componentVersion
      if (!navigation) return accum
      const componentVersionAsciiDocConfig = getAsciiDocConfigWithAsciidoctorReducerExtension(componentVersion)
      const mergedAsciiDocConfig = Object.assign({}, componentVersionAsciiDocConfig, {
        attributes: Object.assign({ revdate }, componentVersionAsciiDocConfig.attributes, assemblerAsciiDocAttributes),
      })
      const rootEntry = { content: title }
      const startPage = contentCatalog.getComponentVersionStartPage(componentName, version)
      let startPageUrl
      if (startPage && insertStartPage) {
        if (includedInNav(navigation, (startPageUrl = startPage.pub.url))) {
          startPageUrl = undefined
        } else {
          Object.assign(rootEntry, { url: startPageUrl, urlType: 'internal' })
        }
      }
      // Q: should we always use a reference page here?
      const startPage_ =
        startPage ??
        createFile({
          component: componentVersion.name,
          version: componentVersion.version,
          relative: '.reference-page.adoc',
          origin: (componentVersion.origins || [])[0],
        })
      const mutableAttributes = selectMutableAttributes(loadAsciiDoc, contentCatalog, startPage_, mergedAsciiDocConfig)
      delete mutableAttributes.doctype
      accum = accum.concat(
        prepareOutlines(navigation, rootEntry, rootLevel).map((outline) =>
          produceAggregateDocument(
            loadAsciiDoc,
            contentCatalog,
            componentVersion,
            outline,
            doctype,
            contentCatalog.getPages((page) => page.out),
            mergedAsciiDocConfig,
            mutableAttributes,
            sectionMergeStrategy
          )
        )
      )
      mergedAsciiDocConfig.attributes.doctype = doctype
      sourceHighlighter
        ? (mergedAsciiDocConfig.attributes['source-highlighter'] = sourceHighlighter)
        : delete mergedAsciiDocConfig.attributes['source-highlighter']
      return accum
    },
    []
  )
}

function createFile (src) {
  const familySegment = (src.family ??= 'page') + 's'
  const path = `modules/${(src.module ??= 'ROOT')}/${familySegment}/${src.relative}`
  const moduleRootPath = Array(src.relative.split('/').length - 1)
    .fill('..')
    .join('/')
  const outPath = [
    src.component === 'ROOT' ? '' : src.component,
    src.version,
    src.module === 'ROOT' ? '' : src.module,
    src.family === 'page' ? '' : '_' + familySegment,
    src.family === 'page' ? src.relative.replace(/\.adoc$/, '.html') : src.relative,
  ]
    .filter((it) => it)
    .join('/')
  return new File({
    path,
    contents: src.contents ?? Buffer.alloc(0),
    src,
    out: { path: outPath },
    pub: { url: '/' + outPath, moduleRootPath },
  })
}

function getAsciiDocConfigWithAsciidoctorReducerExtension (componentVersion) {
  const asciidoctorReducerExtension = require('./asciidoctor/reducer-extension') // NOTE: must be required lazily
  const asciidocConfig = componentVersion.asciidoc
  const extensions = asciidocConfig.extensions || []
  if (extensions.length) {
    return Object.assign({}, asciidocConfig, {
      extensions: extensions.reduce(
        (accum, candidate) => {
          if (candidate !== asciidoctorReducerExtension) accum.push(candidate)
          return accum
        },
        [asciidoctorReducerExtension]
      ),
      sourcemap: true,
    })
  }
  return Object.assign({}, asciidocConfig, { extensions: [asciidoctorReducerExtension], sourcemap: true })
}

function includedInNav (items, url) {
  return items.find((it) => it.url === url || includedInNav(it.items || [], url))
}

// when root level is 0, merge the navigation into the rootEntry
// when root level is 1, create navigation per navigation menu
// in this case, if there's only a single navigation menu with no title, promote each top-level item to a menu
function prepareOutlines (navigation, rootEntry, rootLevel) {
  if (rootLevel === 0 || navigation.length === 1) {
    const navBranch =
      navigation.length === 1
        ? navigation[0]
        : { items: navigation.reduce((navTree, it) => navTree.concat(it.content ? it : it.items), []) }
    return rootLevel === 0 || navBranch.content ? [Object.assign(rootEntry, navBranch)] : navBranch.items
  }
  return navigation.reduce((navTree, it) => navTree.concat(it.content ? it : it.items), [rootEntry])
}

module.exports = produceAggregateDocuments
