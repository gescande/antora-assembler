'use strict'

const convertDocumentToPdf = require('./convert-document-to-pdf')

module.exports.register = function () {
  this.once('contentClassified', ({ contentCatalog }) => {
    contentCatalog.getPages((page) => {
      if (!page.out) return
      page.src.contents = page.contents
      page.src = new Proxy(page.src, { deleteProperty: (o, p) => (p === 'contents' ? true : delete o[p]) })
    })
  })
  this.once('beforePublish', ({ playbook, contentCatalog, siteCatalog }) => {
    const { assembleContent } = this.require('@antora/assembler')
    return assembleContent.call(this, playbook, contentCatalog, convertDocumentToPdf, { siteCatalog })
  })
}
